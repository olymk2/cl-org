(ns cl-eorg.parser-test
  (:require [cljs.test :refer-macros [deftest are is testing]]
            [clojure.string :as str]
            [cl-eorg.tokens :as t]
            [cl-eorg.helpers :as h]
            [cl-eorg.parse-partials :as pp]
            [cl-eorg.parser :as p]))

(def match-test-list ["#+TITLE: bold" [:TITLE " bold"]])

(deftest match-tests
  (are [x y] (= (p/match-token x) y)
    ["#+TITLE: bold"] [:TITLE {:name "TITLE" :content "bold"}]
    ["#+DESCRIPTION: bold"] [:DESCRIPTION {:name "DESCRIPTION" :content "bold"}]
    [" [+] bullet"] [:LINE " [+] bullet"]
    ["* header"] [:HEADER1 " header"]
    ["* TODO do something"] [:HEADER1 [:TODO "TODO"] " do something"]
    ["** header"] [:HEADER2 " header"]
    ["*** header"] [:HEADER3 " header"]
    [""] [:BR]
    ["line of text"] [:LINE "line of text"]))

(deftest parse-lists-test
  (is (= (pp/parse-lists [:BULLETS] :BULLET 2 t/t_BULLET_END [" * [[https://www.google.com]]"])
         [:BULLETS [:BULLET " " [:LINK {:href "https://www.google.com", :title ""} ""]] ()]))

  (is (= (pp/parse-lists [:BULLETS] :BULLET 2 t/t_BULLET_END [" * bullet 1" " * bullet 2"])
         [:BULLETS [:BULLET " bullet 1"] [:BULLET " bullet 2"] ()])))

(deftest parse-lines-until-match-test
  (testing "Test no more lines, so no remainder everything returned"
    (is (= [:LINE "234\n567" ()]
           (h/parse-lines-until-match [:LINE] 0
            (:end (:SOURCE t/org-tokens-map))
            (str/split-lines "123\n234\n567")))))
  (testing "Test last line end tag no extra characters"
    (is (= [:LINE "234\n567" ()]
           (h/parse-lines-until-match [:LINE] 0
            (:end (:SOURCE t/org-tokens-map))
            (str/split-lines "123\n234\n567\n#+END_SRC")))))
  (testing "Test stop when regex matches"
    (is (= [:LINE "234\n567" '("more text" "another line")]
           (h/parse-lines-until-match [:LINE] 0
            (:end (:SOURCE t/org-tokens-map))
            (str/split-lines "123\n234\n567\n#+END_SRC\nmore text\nanother line"))))))

(deftest regex-match-test
  (is (= (h/re-find-token t/t_META "TITLE: bold")
         nil))
  (is (= (h/re-find-token t/t_META "#+TITLE: bold")
         ["#+TITLE:" "TITLE"])))

(deftest match-bullets-test
  (testing "Match ordered list"
    (is (= (p/match-token ["1. bullet 1" "2. bullet 2"])
           [:BULLETS-ORDERED [:BULLET " bullet 1"] [:BULLET " bullet 2"] (list)])))
  (testing "Match unordered list"
    (is (= (p/match-token [" * bullet 1" " * bullet 2"])
           [:BULLETS [:BULLET " bullet 1"] [:BULLET " bullet 2"] (list)])))
  (testing "Match unordered list"
    (is (= (p/match-token ["- bullet 1" "- bullet 2"])
           [:BULLETS [:BULLET " bullet 1"] [:BULLET " bullet 2"] (list)])))
  (testing "Test remainder"
    (is (= (p/match-token ["- bullet 1" "- bullet 2" "" "remainder text"])
           [:BULLETS [:BULLET " bullet 1"] [:BULLET " bullet 2"] (list "" "remainder text")]))))


(deftest match-token-block-test
  (testing "Match correct lines in a block"
    (is (= (p/match-token ["#+BEGIN_SRC sh" "ls -la" "#+END_SRC"])
           [:SRC {:LANGUAGE "sh"} "ls -la" (list)])))
  (testing "Match correct lines in a block, remainder"
    (is (= (p/match-token ["#+BEGIN_SRC sh" "ls -la" "#+END_SRC" "more text" "after"])
           [:SRC {:LANGUAGE "sh"} "ls -la" (list "more text" "after")]))))

(deftest match-token-test
  (testing "match title"
    (is (= (p/match-token ["* TODO lets do something"])
         [:HEADER1 [:TODO "TODO"] " lets do something"]))
    (is (= (p/match-token ["#+TITLE: bold"])
           [:TITLE {:name "TITLE" :content "bold"}]))))

(deftest build-token-from-match-test
  (testing "Test building src token"
    (is (= (p/build-token-from-match
            (:SOURCE t/org-tokens-map)
            12
            ["#+BEGIN_SRC sh" "ls -la" "#+END_SRC"])
           [:SRC {:LANGUAGE "sh"} "ls -la" (list)])))
  (testing "Test building title token"
    (is (= (p/build-token-from-match
            (:META t/org-tokens-map)
            8
            ["#+TITLE: sh"])
           [:TITLE {:name "TITLE" :content "sh"}])))
  (testing "Test building src token with remainder"
    (is (= (p/build-token-from-match
            (:SOURCE t/org-tokens-map)
            12
            ["#+BEGIN_SRC sh" "ls -la" "#+END_SRC" "more" "text"])
           [:SRC {:LANGUAGE "sh"} "ls -la" (list "more" "text")])))
  (testing "Test building multi line src token with remainder"
    (is (= (p/build-token-from-match
            (:SOURCE t/org-tokens-map)
            12
            ["#+BEGIN_SRC sh" "ls -la" "cd ../" "ls -lha" "#+END_SRC" "more" "text"])
           [:SRC {:LANGUAGE "sh"} "ls -la\ncd ../\nls -lha" (list "more" "text")]))))

(deftest match-token-text-test
  (is (=
       (p/match-token ["bold"])
       [:LINE "bold"])))

(comment (p/match-token " [-] tet")
         (p/match-token "#+TEST:")
         (p/match-token "#+TITLE:")
         (p/parse-lines-until-match-end ["ls -la" "#+END_SRC" "test"])
         (p/parse-lines-until-match-end '("ls -la" "#+END_SRC" "test"))
         ((:parse-fn (:SOURCE t/org-tokens-map)) ["#+BEGIN_SRC sh" "ls -la" "#+END_SRC"])
         ((resolve (:parse-fn (:SOURCE t/org-tokens-map))) ["#+BEGIN_SRC sh" "ls -la" "#+END_SRC"])
         (p/match-token "\n"))

