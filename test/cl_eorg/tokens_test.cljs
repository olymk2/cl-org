(ns cl-eorg.tokens-test
  (:require [cljs.test :refer-macros [deftest is are testing]]
            [cl-eorg.parser :as p]
            [cl-eorg.tokens :as t]))

(deftest make-meta-key-test
  (is (= (t/make-meta-key :title 8 "#+title:")
         [:title {:name "title" :content ""}]))
  (is (= (t/make-meta-key :title 8 "#+title: key")
         [:title {:name "title" :content "key"}])))


#_(deftest regex-tests
  (are [token text result]
      (= result
         (last (p/re-find-token (-> t/TOKENS-DICT token :start) text))) 
    :BR "\n" "\n"
    :META "#+TITLE: meta" "TITLE"
    :META "#+DESCRIPTION: meta" "DESCRIPTION"))


