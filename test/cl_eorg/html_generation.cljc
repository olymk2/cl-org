(ns cl-eorg.html-generation
  (:require
   [cl-eorg.parser :as p]
   [cl-eorg.html :as h]
   [cljc.cl-eorg.dsl :refer [generate]]
   #?(:clj [clojure.test :refer [deftest is testing]]
      :cljs [cljs.test :refer-macros [deftest is testing]])
   [cl-eorg.themes.tachyon :refer [tachyon-theme]]
   [cl-eorg.html :refer [org->replacements]]))

(def document1
  (str "#+TITLE: test\n"
       "#+THUMBNAIL:\n"
       "\n"
       "* header 1\n"))

(deftest generate-dsl-test
  (is (= (->> [:LINE "" [:LINK {:href "https://github.com/weavejester/hiccup"} "https://github.com/weavejester/hiccup"]]
              (org->replacements tachyon-theme))
         "#+TITLE: title here\n#+DESCRIPTION: description here")))

(deftest generate-split-header-and-document
  (is (= (h/org->split (p/parse document1))
         {:header [:head [:TITLE {:name "TITLE", :content "test"}]],
          :body [:main [:BR] [:HEADER1 " header 1"]]}
         )))

(h/org->split (p/parse document1))
(drop 1 (h/org->split (p/parse document1)))


