(ns cl-eorg.macros-test
  (:require
   [cljs.test :refer-macros [deftest is testing]]
   [cl-eorg.parser :as p])
  (:require-macros [cl-eorg.macros :refer [inline-resource]]))
