(ns cl-eorg.regex-test
  (:require [cljs.test :refer-macros [deftest is are testing]]
            [cl-eorg.helpers :as h]
            [cl-eorg.tokens :as t]))

(deftest regex-tests
  (are [token text result]
       (= result
          (last (h/re-find-token (-> t/org-tokens-map token :start) text)))
    :BR "\n" "\n"
    :BULLETS " * bullet" "*"
    :BULLETS "* bullet" nil
    :BULLETS "+ bullet" "+"
    :META "#+TITLE: meta" "TITLE"
    :META "#+DESCRIPTION: meta" "DESCRIPTION"))
