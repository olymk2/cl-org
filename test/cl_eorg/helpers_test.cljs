(ns cl-eorg.helpers-test
  (:require
   [cl-eorg.helpers :refer [vector-char-append]]
   [cljs.test :refer-macros [deftest is testing]]))

(deftest vector-char-append-test
  (testing "Last item in vector is a string so append to string"
    (is (= (vector-char-append ["a"] "b")
           ["ab"])))
  (testing "Last item is a string has sub vectors, make sure appended"
    (is (= (vector-char-append ["1" [1 2] "a"] "b")
           ["1" [1 2] "ab"])))
  (testing "Last item is a vector so append the string"
    (is (= (vector-char-append [[1 2]] "b")
           [[1 2] "b"]))))
