(ns cl-eorg.document-test
  (:require
  [cl-eorg.html :refer [org->replacements org->to-hiccup org->split]]
   [cl-eorg.parser :as p]
   #?(:clj [cl-eorg.macros :refer [inline-resource]])
   #?(:clj [clojure.test :refer [deftest is testing]]
      :cljs [cljs.test :refer-macros [deftest is testing]]))
  #?(:cljs (:require-macros [cl-eorg.macros :refer [inline-resource]])))

(def simple-org-document (inline-resource "public/documents/org-simple.org"))
(def simple-org-blocks-document (inline-resource "public/documents/org-blocks.org"))
(def simple-org-src-blocks-document (inline-resource "public/documents/org-src-block.org"))
(def simple-org-bullet-document (inline-resource "public/documents/org-bullets.org"))

(deftest generate-simple-document-test
  (testing "Simple org sample file"
    (is (=
         (p/parse simple-org-document)
         [[:TITLE {:name "TITLE" :content "Simple org Document"}]
          [:DESCRIPTION {:name "DESCRIPTION" :content "File to test parsing "}]
          [:BR]
          [:LINE "" [:LINK {:href "https://github.com/reagent-project/reagent"} "https://github.com/reagent-project/reagent"]]
          [:BR]
          [:LINE "Some intro text"]
          [:BR]
          [:HEADER1 " Top level heading"]
          [:LINE "  Some text under a heading"]
          [:HEADER2 " Second level heading 1"]
          [:HEADER2 " Second level heading 2"]
          [:BR]
          [:HEADER1 [:TODO "TODO"] " do something"]
          [:HEADER1 [:TODO "DONE"] " I did something"]
          [:BR]
          [:LINE "Example Image link"]
          [:LINE " [[image.jpg]]"]
          ;[:LINE [:IMG {:src "image.jpg", :text ""}]]
          [:LINE "Example link to file"]
          [:LINE ""
           [:LINK
            {:href "http://example.com", :title "example.com"}
            "example.com"]]
          [:BR]
          [:HEADER1 " Some text styling"]
          [:LINE "Some "
           [:B "bold"] " text some "
           [:I "italic"] " text some "
           [:U "underlined"] " text "
           [:CODE "text 1"] " "
           [:VERBATIM "text 2"] "."]]))))

(deftest generate-block-document-test
  (testing "Test parsing org blocks"
    (is (=
         (p/parse simple-org-blocks-document)
         [[:TITLE {:name "TITLE" :content "Simple block org Document"}]
          [:DESCRIPTION {:name "DESCRIPTION" :content "File to test parsing blocks"}]
          [:BR]
          [:COMMENT ".. title: test title\n.. type: text"]
          [:BR]
          [:LINE "Some intro text"]
          [:BR]
          [:SRC {:LANGUAGE "sh"} "ls -la\ncd ../\nls -lha"]
          [:EXAMPLE "example block"]])))
  (testing "Test parsing org blocks"
    (is (= (org->to-hiccup (p/parse simple-org-blocks-document))
           [:main
            [:header.f2.fw6.f2-ns.lh-title.mt0.mb2 {:name "TITLE", :content "Simple block org Document"}]
            [:meta {:name "DESCRIPTION", :content "File to test parsing blocks"}]
            [:br]
            [:br]
            [:p.f5.f4-ns.lh-copy.mt0 "Some intro text"]
            [:br]
            [:pre.bg-near-black.silver.pa2.hljs.roboto.overflow-auto.klipse {:class "sh"} "ls -la\ncd ../\nls -lha"] [:pre.bg-near-black.silver.pa2.hljs.roboto.overflow-auto "example block"]]
    ))
  )
          
          )

(deftest generate-src-block-document-test
  (testing "Test org src block"
    (is (=
         (p/parse simple-org-src-blocks-document)
         [[:LINE "some text"]
          [:SRC {:LANGUAGE "clojure"} "  ;; lists are denoted with brackets, but remember to use '(\n  ;; or (list function) else the first value is a function call.\n  '(1 2 3)\n\n  ;; vectors are donoted with square brackets\n  [1 2 3 4]\n\n  ;; hash-maps are denoted with curly braces\n  ;; they must always contain key value pairs.\n  {:key-one 1 :key-two 2}\n\n  ;; sets also use curly braces but start with a #\n  ;; sets have to be unique so duplicates will be removed\n  #{1 2 1 3}"]])))
  (testing "Test org src block to html"
    (is (= (org->to-hiccup (p/parse simple-org-src-blocks-document))
           [:main
            [:p.f5.f4-ns.lh-copy.mt0 "some text"]
            [:pre.bg-near-black.silver.pa2.hljs.roboto.overflow-auto.klipse {:class "clojure"} "  ;; lists are denoted with brackets, but remember to use '(\n  ;; or (list function) else the first value is a function call.\n  '(1 2 3)\n\n  ;; vectors are donoted with square brackets\n  [1 2 3 4]\n\n  ;; hash-maps are denoted with curly braces\n  ;; they must always contain key value pairs.\n  {:key-one 1 :key-two 2}\n\n  ;; sets also use curly braces but start with a #\n  ;; sets have to be unique so duplicates will be removed\n  #{1 2 1 3}"]]))))


(deftest generate-bullets-document-test
  (testing "Test org src block"
    (is (=
         (p/parse simple-org-bullet-document)
         [[:LINE "Test 1"]
          [:BULLETS-ORDERED [:BULLET " bullet 1"] [:BULLET " bullet 2"]]
          [:LINE "Test 2"]
          [:BULLETS-ORDERED [:BULLET " bullet 1"] [:BULLET " bullet 2"]]
          [:LINE "Test 3"]
          [:BULLETS [:BULLET " bullet 1"] [:BULLET " bullet 2"]]
          [:LINE "Test 4"]
          [:BULLETS [:BULLET " bullet 1"] [:BULLET " bullet 2"]]
          [:LINE "Test 5"]
          [:BULLETS [:BULLET " bullet 1"] [:BULLET " bullet 2"]]
          [:LINE "Test 6 should not be bullets"]
          [:HEADER1 " bullet 1"]
          [:HEADER1 " bullet 2"]]))))




[[:TITLE {:name "TITLE", :content "Simple org Document"}]
 [:DESCRIPTION
  {:name "DESCRIPTION", :content "File to test parsing "}]
 [:BR]
 [:LINE
  ""
  [:LINK
   {:href "https://github.com/reagent-project/reagent"}
   "https://github.com/reagent-project/reagent"]]
 [:BR]
 [:LINE "Some intro text"]
 [:BR]
 [:HEADER1 " Top level heading"]
 [:LINE "  Some text under a heading"]
 [:HEADER2 " Second level heading 1"]
 [:HEADER2 " Second level heading 2"]
 [:BR]
 [:HEADER1 [:TODO "TODO"] " do something"]
 [:HEADER1 [:TODO "DONE"] " I did something"]
 [:BR]
 [:LINE "Example Image link"]
 [:LINE " [[image.jpg]]"]
 [:LINE "Example link to file"]
 [:LINE
  ""
  [:LINK
   {:href "http://example.com", :title "example.com"}
   "example.com"]]
 [:BR]
 [:HEADER1 " Some text styling"]
 [:LINE
  "Some "
  [:B "bold"]
  " text some "
  [:I "italic"]
  " text some "
  [:U "underlined"]
  " text "
  [:CODE "text 1"]
  " "
  [:VERBATIM "text 2"]
  "."]]


[[:TITLE {:name "TITLE", :content "Simple org Document"}]
 [:DESCRIPTION
  {:name "DESCRIPTION", :content "File to test parsing "}]
 [:BR]
 [:LINE
  ""
  [:LINK
   {:href "https://github.com/reagent-project/reagent"}
   "https://github.com/reagent-project/reagent"]]
 [:BR]
 [:LINE "Some intro text"]
 [:BR]
 [:HEADER1 " Top level heading"]
 [:LINE "  Some text under a heading"]
 [:HEADER2 " Second level heading 1"]
 [:HEADER2 " Second level heading 2"]
 [:BR]
 [:HEADER1 [:TODO "TODO"] " do something"]
 [:HEADER1 [:TODO "DONE"] " I did something"]
 [:BR]
 [:LINE "Example Image link"]
 [:LINE " " [:LINK {:href "image.jpg", :title "", :img true}]]
 [:LINE "Example link to file"]
 [:LINE
  ""
  [:LINK
   {:href "http://example.com", :title "example.com"}
   "example.com"]]
 [:BR]
 [:HEADER1 " Some text styling"]
 [:LINE
  "Some "
  [:B "bold"]
  " text some "
  [:I "italic"]
  " text some "
  [:U "underlined"]
  " text "
  [:CODE "text 1"]
  " "
  [:VERBATIM "text 2"]
  "."]]
[[:TITLE {:name "TITLE", :content "Simple org Document"}]
 [:DESCRIPTION
  {:name "DESCRIPTION", :content "File to test parsing "}]
 [:BR]
 [:LINE
  ""
  [:LINK
   {:href "https://github.com/reagent-project/reagent"}
   "https://github.com/reagent-project/reagent"]]
 [:BR]
 [:LINE "Some intro text"]
 [:BR]
 [:HEADER1 " Top level heading"]
 [:LINE "  Some text under a heading"]
 [:HEADER2 " Second level heading 1"]
 [:HEADER2 " Second level heading 2"]
 [:BR]
 [:HEADER1 [:TODO "TODO"] " do something"]
 [:HEADER1 [:TODO "DONE"] " I did something"]
 [:BR]
 [:LINE "Example Image link"]
 [:LINE " [[image.jpg]]"]
 [:LINE [:IMG {:src "image.jpg", :text ""}]]
 [:LINE "Example link to file"]
 [:LINE
  ""
  [:LINK
   {:href "http://example.com", :title "example.com"}
   "example.com"]]
 [:BR]
 [:HEADER1 " Some text styling"]
 [:LINE
  "Some "
  [:B "bold"]
  " text some "
  [:I "italic"]
  " text some "
  [:U "underlined"]
  " text "
  [:CODE "text 1"]
  " "
  [:VERBATIM "text 2"]
  "."]]
