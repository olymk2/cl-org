(ns cl-eorg.links-test
  (:require
   [cl-eorg.emphasis :as e :refer [emphasis match-char parse-line]]
   [cl-eorg.parser :as p]
   [cljs.test :refer-macros [deftest is testing]]))

(deftest match-link-in-text-test
    (is (= (p/parse " * test")
         [[:BULLETS [:BULLET " test"]]]
         #_[[:LINE "" [:LINK {:href "https://www.google.com"} "https://www.google.com"]]]))

  (is (= (p/parse " * [[https://www.google.com]]")
         [[:BULLETS [:BULLET " " [:LINK {:href "https://www.google.com", :title ""} ""]]]]))

  (is (= (p/parse "test \nhttps://www.google.com")
         [[:LINE "test "] [:LINE "" [:LINK {:href "https://www.google.com"} "https://www.google.com"]]]))

  (is (= (p/parse " https://www.google.com")
         [[:LINE " " [:LINK {:href "https://www.google.com"} "https://www.google.com"]]]))

  (is (= (p/parse "test text with link https://www.google.com")
         [[:LINE "test text with link " [:LINK {:href "https://www.google.com"} "https://www.google.com"]]])))
