(ns cl-eorg.dsl-test
  (:require
   [cljc.cl-eorg.dsl :refer [generate]]
   [cljs.test :refer-macros [deftest is testing]]))

(deftest generate-dsl-test
  (is (= (generate
        [[:TITLE "title here"]
         [:DESCRIPTION "description here"]])
       "#+TITLE: title here\n#+DESCRIPTION: description here")))
