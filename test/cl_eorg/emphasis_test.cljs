(ns cl-eorg.emphasis-test
  (:require
   [cl-eorg.emphasis :as e :refer [emphasis match-char parse-line]]
   [cljs.test :refer-macros [deftest is testing]]))

(deftest emphasis-test
  (testing "Test end char does not end unless there is a space after or line end"
    (is (= (emphasis :m \= (seq "=cd="))
           [[:m "cd"]]))
    (is (= (emphasis :m \= (seq "=cd=ed"))
           '(\= \c \d \= \e \d))))


  (testing "Starts with match and has a match"
    (is (= (emphasis :m \= (seq "=cd= ed"))
           [[:m "cd"] " ed"])))
  (testing "Starts with match and has no ending match"
    (is (= (emphasis :m \= (seq "=cded"))
           '(\= \c \d \e \d))))
  (testing "Starts with match and has no ending match"
    (is (= (emphasis :m \= (seq "cded"))
           '(\c \d \e \d)))))

(deftest match-image-test
  (is (= (e/match-image :IMG \[ \] (seq "[[123abc]] abc"))
         [[:IMG "123abc"] " abc"])))

(deftest match-internal-link-test
  (is (= (e/match-internal-link :LINK \[ \] (seq "[[../../../images/opengl/point-sprite-shader.png]]"))
         [[:LINK {:href "../../../images/opengl/point-sprite-shader.png", :title "", :img true}] ""]))

  (is (= (e/match-internal-link :LINK \[ \] (seq "[[link]]"))
         [[:LINK {:href "link", :title ""} ""] ""]))
  (is (= (e/match-internal-link :A \[ \] (seq "[[link.jpg]]"))
         [[:A {:href "link.jpg", :title "" :img true}] ""]))
  (is (= (e/match-internal-link :A \[ \] (seq "[[link][description]]"))
         [[:A {:href "link", :title "description"} "description"] ""])))

(deftest match-char-test
  (is (= (match-char (seq "=a= ab ed"))
         ["" [:VERBATIM "a"] " ab ed"]))
  (is (= (match-char (seq "ab =cd= ed"))
         ["ab " [:VERBATIM "cd"] " ed"])))

(deftest parse-line-test
  (is (= (parse-line (seq "[[link.jpg]]"))
         ["" [:LINK {:href "link.jpg" :title "" :img true}]]))
(is (= (parse-line (seq " [[link.jpg]]"))
         [" " [:LINK {:href "link.jpg" :title "" :img true}]]))
  (is (= (parse-line (seq "hi there *bold* _underlined_ /italic/"))
         ["hi there " [:B "bold"] " " [:U "underlined"] " " [:I "italic"]]))
  (is (= (parse-line (seq "hi there =equals pre= more text"))
         ["hi there " [:VERBATIM "equals pre"] " more text"])))


(comment
(match-char (seq "[[link.jpg]]"))

)
