(ns cl-eorg.helpers
  (:require [clojure.string :as str]))

(def rm-ns (comp keyword name))
(def empty-vector? #(when (vector? %) (empty? %)))
(def not-empty-vector? #(when (vector? %) ((complement empty?)  %)))

(defn drop-chars
  "Remove X many chars from a string."
  [amount text]
  (apply str (drop amount text)))

(defn re-find-token
  "just match the supplied regex against a string"
  [regex text]
  (-> regex (re-find text)))

(defn match-word [match words]
  (-> (str "^(" (clojure.string/join "|" words) ")")
      (re-pattern)
      (re-find-token match)
      first))

(defn concat-string-unless-empty
  "concat to the token if non empty string"
  [token letters]
  (if (= 0 (count letters))
    token
    (conj token (clojure.string/join "" letters))))

(defn concat-vector-or-string
  "concat to the token based on type"
  [token token-body]
  (if (nil? token-body)
    token
    (if (vector? token-body)
      (vec (concat token token-body))
      (conj token token-body))))

(defn vector-char-append
  "Given a vector append to end, if last is a string then add to string."
  [result char]
  (if (string? (last result))
    (conj (vec (butlast result))
          (str (last result) char))
    (conj result
          (str (when (string? (last result)) (last result)) char))))

(defn parse-lines-until-match
  "Given a regex and a sequence of strings, collect text until regex regex end"
  [token match-len regex text]
  (when (seq text)
    (loop [lines (rest text)
           result []]
      (if (or (empty? lines) (re-find-token regex (first lines)))
        (concat-vector-or-string token [(clojure.string/join "\n" result) (rest lines)])
        (recur (rest lines)
               (conj result (first lines)))))))

(defn org-str->keyword
  "parse a string beginning with a : into a keyword"
  [org-keyword]
  (keyword (apply str (drop 1 org-keyword))))

(defn parse-attribs
  "Parse keyword values in a string to a hash map"
  [attribs]
  (loop [result {}
         parse-attribs (if (string? attribs)
                         (clojure.string/split attribs #"\s")
                         attribs)]
    (if (seq parse-attribs)
      (recur
       (case (first (first parse-attribs))
         ":" (assoc result (org-str->keyword (first parse-attribs)) (second parse-attribs))
         result)
       (rest parse-attribs))
      result)))

(defn parse-src-attribs [attribs]
  (merge {:LANGUAGE  (first attribs)}
         (parse-attribs (rest attribs))))

(defn parse-src-lines-until-match
  "Given a regex and a sequence of strings, collect text until regex end"
  [token match-len regex text]
  (concat-vector-or-string
   token
   (vec (concat [(parse-src-attribs  (clojure.string/split (drop-chars match-len (first text)) #"\s"))]
                (parse-lines-until-match nil match-len regex text)))))

(defn parse-skip "" [token & _] token)

(defn parse-todo
  [token match-len line todo]
  (let [m (match-word (apply str (drop match-len (first line))) ["TODO" "DONE"])]
    (concat-vector-or-string
     token
     (if m
       [[:TODO m] (apply str (drop (+ match-len (count m)) (first line)))]
       (apply str (drop (- match-len 1) (first line)))))))

(defn make-table-row [row]
  (reduce (fn [r cell] (conj r [:TD cell])) [:TR]
          (clojure.string/split (drop-chars 1 row) #"\|")))

(defn parse-table
  "Give an org format table seperated by pipes, build up a html style representation"
  [token match-len regex rows]
  (loop [tbl []
         block [:THEAD]
         lines rows]
    (if (= \| (first (first lines)))
      (if (= \- (second (first lines)))
        (recur
         (conj tbl block)
         [:TBODY]
         (rest lines))
        (recur
         tbl
         (conj block (make-table-row (first lines)))
         (rest lines)))
      (concat-vector-or-string
       token
       (conj (conj tbl block) lines)))))

(comment
  (re-find-token #"^(TODO|DONE)" "TODO get something done")
  (re-find-token #"^(TODO|DONE)" "DONE get something done")
  (re-find-token #"^(TODO|DONE)" "abc TODO DONE get something done")
  (parse-table 0 #"" ["| header1 | header2 | header3 |" "| cell1 | cell2 | cell3 |" "more text"])
  (parse-table 0 #"" ["| header1 | header2 | header3 |"
                      "|---+----+----|"
                      "| cell1 | cell2 | cell3 |" "more text"])
  (first "| abc")
  (match-word "TODO get something done" ["TODO" "DONE"])
  (match-word "DONE get something done" ["TODO" "DONE"])
  (match-word "a DONE get something done" ["TODO" "DONE"])
  (parse-attribs  "#+BEGIN_SRC shell :exports results")
  (parse-src-lines-until-match 12 #"test" ["#+BEGIN_SRC shell :exports results" "new line"])
  (apply str (take 4 "todo kjhk")))

