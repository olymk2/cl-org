(ns cl-eorg.parse-partials
  (:require [cl-eorg.helpers :refer [drop-chars re-find-token]]
            [cl-eorg.emphasis :refer [parse-line]]))

(defn return-string-or-list [tokens]
  (if (and (= 1 (count tokens)) (string? (first tokens)))
    (first tokens)
    tokens)
  
  #_(if (string? (first tokens))
    
    (vec (concat [:TEXT] tokens))
    tokens))

(defn parse-lists
  "Given a regex and a sequence of strings, collect text until regex regex end"
  [token key match-len regex text]
  (when (seq text)
    (loop [lines text
           result token]
      (if (or (empty? lines) (re-find-token regex (first lines)))
        (vec (conj result lines))
        (recur (rest lines)
               (conj result (vec (concat [key ] (parse-line (drop-chars match-len (first lines)))))))))))

(comment (parse-line " [[https://www.google.com]]"))
