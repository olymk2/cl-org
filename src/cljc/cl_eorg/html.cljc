(ns cl-eorg.html
  (:require [cl-eorg.themes.tachyon :refer [tachyon-theme]]
            [cl-eorg.tokens :as t]
            [clojure.walk :refer [postwalk-replace prewalk-replace prewalk postwalk walk]]))

(def headers (partial contains? (into #{} (map keyword t/METADATA))))
(def body (complement headers ))


(defn fn-or-string [default smap form]
  (let [value (get smap (first form) (first form) #_default)]
    (cond
      (fn? value) (value form) ;(assoc form 0 (value form))
      (nil? value) nil
      :else (assoc form 0 value))))

(def first-keyword? (fn [x] (when (vector? x) (keyword? (first x)))))

(defn postwalk-replace-default
  "Like postwalk replace, but replaces any keyword uses a default when missing"
  {:added "1.1"}
  [default smap form]
  (->> form
       (postwalk
        (fn [x] (if (first-keyword? x)
                  (fn-or-string default smap x)
                  x)))
       (remove nil?)
       vec))

(defn org->replacements
  "Match keywords replacing with hiccup keywords"
  ([dsl] (org->replacements tachyon-theme dsl))
  ([theme dsl]
   (postwalk-replace theme dsl)))

(defn org->split
  "Split out meta and body"
  [dsl]
  {:header (into [:head] (filter (fn filter-headers [tag] (headers (first tag))) dsl))
   :body   (into [:main] (filter (fn filter-body [tag] (body (first tag))) dsl))})

(defn org->to-hiccup
  "Match keywords replacing with hiccup keywords"
  ([dsl] (org->to-hiccup tachyon-theme dsl))
  ([theme dsl]
   (->> dsl
        org->split
        :body
        (postwalk-replace-default :div.not-handled theme)
        vec)))
