(ns cl-eorg.parser
  (:require [cl-eorg.emphasis :refer [parse-line]]
            [cl-eorg.helpers :refer [re-find-token]]
            [cl-eorg.tokens :as tokens]
            [clojure.string :as str]))

(defn re-token-match
  "just match the supplied regex against a string"
  [{:keys [start key key-fn] :or {key-fn identity key nil}} text]
  (when-let [s (-> start (re-find text))]
    [(or key (key-fn (first s)))
     (subs text (count (first s)))]))

(defn make-key-default
  "Make the keyword token"
  [{:keys [key]} _ _]
  (when key [key]))

(defn parse-line-rest
  "Get rest of the line"
  [token match-len regex lines]
  (vec (concat token (subs (first lines) match-len))))

(defn concat-vector-or-string
  "concat to the token based on type"
  [token token-body]
  (if (nil? token-body)
    token
    (if (vector? token-body)
      (vec (concat token token-body))
      (conj token token-body))))

(defn build-token-from-match
  "return tokens key and parsed data."
  [{:keys [key-fn parse-fn]
    :as token
    :or {key-fn make-key-default
         parse-fn parse-line-rest}}
   match-len lines]
  (parse-fn
   (key-fn token match-len (first lines))
  match-len (:end token) lines)
  #_(->
   (key-fn token match-len (first lines))
   (concat-vector-or-string
    (parse-fn match-len (:end token) lines))))

(defn match-token
  "Match a specific token or from a list of tokens"
  ([lines]
   (let [result
         (reduce (fn [_ [kw token]]
                   (when-let [match (re-find-token (:start token) (first lines))]
                     (reduced (build-token-from-match
                               token
                               (count (if (string? match) match (first match)))
                               lines))))
                 []
                 tokens/org-tokens)]
     (if result result (vec (concat [:LINE] (parse-line (first lines)))))))
  ([lines token] (match-token lines token :start))
  ([lines token key]
   (let [match (re-find-token (key token) lines)]
     (if match
       (build-token-from-match
        token
        (count (if (string? match) match (first match)))
        lines)
       ;; no match return text
       [lines]))))

(defn parse
  "Parse a block of org text"
  [text]
  (loop [result []
         lines (str/split-lines text)]
    (if (seq lines)
      (let [match (match-token lines)]
        (if (seq? (last match))
          (recur (conj result (vec (butlast match)))
                 (rest (last match)))
          (recur (conj result match)
                 (rest lines))))
      result)))

(defn parse-flat
  "A simple parser, used for testing mainly."
  [text]
  (parse text))

(defn filter-root-nodes
  "Match the keys at the root, handy for pulling out meta data"
  ([dsl] (filter-root-nodes dsl nil))
  ([extra dsl]
   (->> dsl
        (filter #(contains?
                  (into #{} (map keyword (concat tokens/METADATA extra)))
                  (first %)))
        (map (fn [node] [(-> node first name clojure.string/lower-case keyword)
                         (:content (second node))]))
        (into {}))))


