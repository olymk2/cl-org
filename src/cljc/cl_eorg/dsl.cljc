(ns cljc.cl-eorg.dsl)

(def dsl-test3
  [[:TITLE "test"]
   [:HEADER1 "header1"]
   [:P "lorem ipsum"]
   [:HEADER2 "header2"]
   [:P "lorem ipsum"]])

(defn item [row]
  (case (first row)
    :TITLE (str "#+TITLE: " (last row) )
    :DESCRIPTION (str "#+DESCRIPTION: " (last row) )
    :HEADER1 (str "* " (last row) )
    :HEADER2 (str "** " (last row) )
    :P (str (last row) )
    (last row)))

(defn generate [doc]
  (clojure.string/join "\n" (map item doc)))

(comment (generate dsl-test3))
