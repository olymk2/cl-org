(ns cl-eorg.emphasis
  (:require [cl-eorg.helpers :refer [vector-char-append concat-string-unless-empty]]))

(def rest2 (comp rest rest))
(def third (comp first rest rest))
(defn skip-space [text] (if (= " " (first text)) (rest text) text))
(defn match-seq-start?
  "given a string or seq of chars, match against the beginning of the second sequence"
  [match text]
  (= (take (count match) (seq text))
     (seq match)))

(defn match-seq-url?
  "Combine http and https matching"
  [text]
  (or (match-seq-start? "http://" text)
      (match-seq-start? "https://" text)))

(defn match-image-ext [image]
  (some #(clojure.string/ends-with? image %) ["jpg" "png"]))

(defn match-external-link
  "Links starting with http and ot [["
  [key text]
  (if (match-seq-url? (skip-space text))
    (let [[url remainder] (split-with #(not= " " %) (skip-space text))]
      [[key {:href (clojure.string/join "" url)} (clojure.string/join "" url)] remainder])
    text))

(defn match-internal-link
  "Match-Open the starting char, keep going until the same char is encountered
  return the match-open and rest, else return the char sequence"
  [key match-open match-close text]
  (if (and (< 2 (count text))
           (= match-open (first text))
           (= match-open (second text)))

    (loop [letters (rest (rest text))
           attrib :href
           description {:href "" :title ""}
           text-markup ""]

      (if (seq letters)
        (if (= (list match-close match-close) (take 2 letters))
          ;; matched end so return
          (if (match-image-ext (:href description))
            [[key (-> description
                      ;(assoc :alt (clojure.string/join "" (rest2 letters)))
                      (assoc :img true))]
             (clojure.string/join "" (rest2 letters))]
            [[key description (:title description)] (clojure.string/join "" (rest2 letters))])
          (if (= (list match-close match-open) (take 2 letters))
            ;; skip 2 characters when close and open bracket, change attribute
            (recur (rest2 letters)
                   :title
                   description
                   text-markup)
            ;; just append to current attribute
            (recur (rest letters)
                   attrib
                   (assoc description attrib (str (attrib description) (first letters)))
                   (str text-markup (first letters)))))
        text))
    text))

(defn match-image
  "Match the starting char, keep going until the same char is encountered
  return the match and rest, else return the char sequence"
  [key match match-close text]
  (if (and (< 2 (count text))
           (= match (first text))
           (= match (second text)))
    (loop [letters (rest (rest text))
           text-markup ""]
      (if (seq letters)
        (if  (= (list match-close match-close) (take 2 letters))
          [[key text-markup] (clojure.string/join "" (rest2 letters))]
          (recur (rest letters)
                 (str text-markup (first letters))))
        text))
    text))

(defn fetch-token-end-remainder
  "Tokens are a list and remainder string, if no remainder terminate"
  [token]
  (if (= 1 (count token)) nil (last token)))

(defn match-end-char?
  "Match char has a space or is last char in sequence"
  [match letters]
  (or
   (and (= match (first letters))
        (= " " (second letters)))
   (and (= match (first letters)) (= 1 (count letters)))))

(defn emphasis
  "Match the starting char, keep going until the same char is encountered
  return the match and rest, else return the char sequence"
  [key match text]
  (if (seq text)
    (loop [letters (if (= " " (first text))
                     (rest (rest text))
                     (rest text))
           text-markup ""]
      (if (seq letters)
        (if (match-end-char? match letters)
          (concat-string-unless-empty [[key text-markup]] (rest letters))
          ;[[key text-markup] (clojure.string/join "" (rest letters))]
          (recur (rest letters)
                 (str text-markup (first letters))))
        text))
    text))

(defn match-char
  "Match start characters "
  [text]
  (loop [letters text
         start true
         result []]
    (if (seq letters)
      (let [l (if (= " " (first letters)) " " nil)
            match (if (or start
                      (= " " (first letters)))
                ;; these must start with a space
                (case (if start (first letters) (second letters))
                  "*" (emphasis :B \* letters)
                  "/" (emphasis :I \/ letters)
                  "_" (emphasis :U \_ letters)
                  "=" (emphasis :VERBATIM \= letters)
                  "~" (emphasis :CODE \~ letters)
                  "+" (emphasis :STRIKE \+ letters)
                  "[" (match-internal-link :LINK \[ \] letters)
                  "h" (match-external-link :LINK letters)
                  letters)
                ;; these can sit next to any character
                (case (first letters)
                  "[" (match-internal-link :LINK \[ \] letters)
                  "h" (match-external-link :LINK letters)
                  letters))]
        (if (vector? match)
          (recur (fetch-token-end-remainder match) false (conj (vector-char-append result l) (first match) ))
          (recur (rest match) false (vector-char-append result (first match)))))
      result)))

(defn parse-line
  "Parse a single line of text"
  [text]
  (match-char (seq text)))

(comment
  (match-external-link :A (seq "http://example.com test"))
  (match-external-link :A (seq " https://example.com test"))
  (parse-line (seq "test /abc/ abc"))
  (parse-line (seq "*abc*"))
  (parse-line  "*abc*")
  (parse-line (seq "test https://example.com test"))
  (parse-line (seq "https://example.com test"))
  (parse-line (seq " https://example.com test"))
  (parse-line (seq " [[https://example.com test]]"))
  (parse-line (seq " [[https://example.com test][test]]"))
  (parse-line (seq "[[https://example.com test]]"))
  (match-external-link :A (seq " https://example.com"))
  (match-external-link :A (seq "https://example.com"))
  (match-internal-link :A \[ \] (seq "[[https://example.com]] other stuff"))
  (match-internal-link :A \[ \] (seq "[[test.jpg]]"))
  (match-image-ext "test.jpg")
  (match-image-ext "test.jpeg")
  (parse-line "ab=1223=*italic* text")
  (first (rest (seq " [+] bullet")))
  (parse-line "bullet")
  (parse-line " [] bullet")
  (parse-line " [+] bullet")
  (emphasis :STRIKE \+ (seq "+1223+"))
  (first (emphasis :STRIKE \+ (seq " + 1223")))
  (emphasis :STRIKE \+ (seq "+] bullet"))
  (emphasis :STRIKE \+ (seq " + 1223"))
  (parse-line (rest (seq " [+] bullet"))))
