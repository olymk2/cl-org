(ns cl-eorg.tokens
  (:require [clojure.string :refer [join]]
            [cl-eorg.parse-partials :refer [parse-lists]]
            [cl-eorg.helpers :refer [parse-todo parse-lines-until-match
                                     parse-src-lines-until-match
                                     parse-skip
                                     parse-table]]))

(def ESCAPE "\n")

(def METADATA  ["TITLE"
                "AUTHOR"
                "EMAIL"
                "DESCRIPTION"
                "KEYWORDS"
                "FILETAGS"
                "THUMBNAIL"
                "DATE"
                "HTML_DOCTYPE"
                "SETUPFILE"])

(def t_META (re-pattern (str "^[#]\\+(" (join "|" METADATA) ")\\:")))
;(def t_META   #"^[#]\+(TITLE|AUTHOR|EMAIL|DESCRIPTION|KEYWORDS|FILETAGS|DATE|HTML_DOCTYPE|SETUPFILE)\:")
(def t_BLANK_LINE  #"^\s*$")
(def t_COMMENT_BEGIN #"^\#\+BEGIN_COMMENT")
(def t_COMMENT_END #"^\#\+END_COMMENT")
(def t_EXAMPLE_BEGIN #"^\#\+BEGIN_EXAMPLE")
(def t_EXAMPLE_END  #"^\#\+END_EXAMPLE")
(def t_SRC_BEGIN  #"^\#\+BEGIN_SRC\s+")
(def t_SRC_END  #"^\#\+END_SRC")
(def t_TABLE_START  #"^\s*\|")
(def t_TABLE_END  #"^(?!\s*\|).*$")
(def t_RESULTS_START  #"^\#\+RESULTS\:")
(def t_CAPTIONS  #"^\#\+CAPTION:")
(def t_NAME  #"^\#\+NAME:")
; t_IMG  #"^\[\[(\w|\.|-|_|/)+\]\]$"
(def t_IMG  #"^\[\[")
(def t_IMG_END  #"[^\]]*")
(def t_RESULTS_END  #"^\s*$")
;(def t_END_LABELS  #"^(?!\[|\#).*")
(def t_BULLET_ORDERED_START  #"^\s*[0-9][\.\)]")
(def t_BULLET_UNORDERED_START  #"^\s*[\+|\-]|^\ +[\*]")
#_(def t_BULLET_START  #"^\s*[\+|\-|0-9\.]")
(def t_BULLET_END  #"^(?!\s*[\+|\-|0-9\.]|^\ +[\*]).*$")

(def t_HEADER  #"^\*+\s")
(def t_META_OTHER  #"^[#]\+[A-Z\_]+\:")

(defn token
  ([token-type value] (token token-type value nil))
  ([token-type value attrs]
   {:token token-type :value (or value "") :attrs attrs}))

(defn make-meta-key [_ match-len text]
  (let [name (clojure.string/join (subs text 2 (- match-len 1)))
        content-start (+ match-len 1)]
    [(keyword name) {:name name
                     :content
                     ;; check we have some content, we can't subs from the end
                     (if (>= content-start (count text))
                       ""
                       (subs text (+ match-len 1)))}]))

(defn make-header-key [_ match-len _]
  [(keyword (str "HEADER" (- match-len 1)))])

(def org-tokens
  [[:META {:start t_META, :key-fn #(cl-eorg.tokens/make-meta-key %1 %2 %3)
           :parse-fn #(parse-skip %1 %2 %3 %4)}]
   [:COMMENT {:start t_COMMENT_BEGIN, :end t_COMMENT_END, :key :COMMENT
              :parse-fn #(parse-lines-until-match %1 %2 %3 %4)}]
   [:EXAMPLE {:start t_EXAMPLE_BEGIN, :end t_EXAMPLE_END, :key :EXAMPLE
              :parse-fn #(parse-lines-until-match %1 %2 %3 %4)}]
   [:CAPTION {:start t_CAPTIONS :key :CAPTION}]
   [:BR {:start t_BLANK_LINE, :key :BR :parse-fn #(parse-skip %1 %2 %3 %4)}]

   [:SOURCE {:start t_SRC_BEGIN, :end t_SRC_END :key :SRC
             :parse-fn #(parse-src-lines-until-match %1 %2 %3 %4)}]
   [:TABLE {:start t_TABLE_START, :end t_TABLE_END :key :TABLE :parse-fn #(parse-table %1 %2 %3 %4)}]
   [:BULLETS-ORDERED {:start t_BULLET_ORDERED_START, :end t_BULLET_END :key :BULLETS-ORDERED
              :parse-fn #(parse-lists %1 :BULLET 2 %3 %4)}]
   [:BULLETS {:start t_BULLET_UNORDERED_START, :end t_BULLET_END :key :BULLETS
                 :parse-fn #(parse-lists %1 :BULLET %2 %3 %4)}]

   [:RESULTS {:start t_RESULTS_START, :end t_RESULTS_END :key :RESULTS}]
   [:HEADER {:start t_HEADER, :key-fn #(make-header-key %1 %2 %3)
             :parse-fn #(parse-todo %1 %2 %4 %3)}]
   [:META_OTHER {:start t_META_OTHER :key-fn #(cl-eorg.tokens/make-meta-key %1 %2 %3)
                 :parse-fn #(parse-skip %1 %2 %3 %4)}]])

(def org-tokens-map (into {} org-tokens))

