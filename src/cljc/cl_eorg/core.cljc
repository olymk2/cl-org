(ns cl-eorg.core
  (:require
   [cl-eorg.parser :as p]))

(defn parse
  "A simple parser, used for testing mainly."
  [text]
  (p/parse text))
