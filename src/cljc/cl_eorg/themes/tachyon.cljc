(ns cl-eorg.themes.tachyon
  (:require
   [clojure.set :refer [rename-keys]]
   [clojure.string]))

(defn link-handler [v _]
  (if (-> v second :img)
    (conj [:img] (dissoc (rename-keys (second v) {:href :src}) :img))
    (conj (conj [:a] (second v)) (or (when-not (clojure.string/blank? (last v)) (last v))
                                     (:href (second v))
                                     (:title (second v))))))

(def tachyon-theme
  {:TITLE :header.f2.fw6.f2-ns.lh-title.mt0.mb2
   ;:DESCRIPTION :p
   :HEADER1 :h1.f3.fw6.f3-ns.lh-title.mt0.mb2
   :HEADER2 :h2.f3.fw6.f3-ns.lh-title.mt0.mb2
   :HEADER3 :h3
   :HEADER4 :h4
   :HEADER5 :h5
   :TODO :span
   :VERBATIM :code
   :CODE :code

   :MAIN :main

   :THUMBNAIL nil
   :DESCRIPTION :meta ;(fn [v] [:span {:name (str v)} ])
   :CATEGORY :meta ;(fn [v] [:span {:name (str v)} ])
   :SLUG :meta ;(fn [v] [:span {:name (str v)} ])
   :DATE :meta ;(fn [v] [:span {:name (str v)} ])
   :FILETAGS :meta ;(fn [v] [:span {:name (str v)} ])
   :NAME :meta ;(fn [v] [:span {:name (str v)} ])

   :TABLE :table.collapse.ba.br2.b--black-10.pv2.ph3
   :TBODY :tbody
   :THEAD :thead
   :TR :tr.striped--light-gray
   :TD :td.pv2.ph3

   ;;TODO this is a src attrib probably worth handling this differently
   ;;perhaps allow custom functions instead of a simple replace
   :LANGUAGE :class
   :SRC :pre.bg-near-black.silver.pa2.hljs.roboto.overflow-auto.klipse
   :RESULTS :pre.bg-near-black.silver.pa2.hljs.roboto.overflow-auto
   :EXAMPLE :pre.bg-near-black.silver.pa2.hljs.roboto.overflow-auto
   :COMMENT nil
   :CAPTION :span
   :I :i
   :U :u
   :B :b
   :LINK link-handler
   :IMG :img
   ;; probably remove these 2
   :A :a
   :P :p
   :BR :br
   :LINE :p.f5.f4-ns.lh-copy.mt0
   :BULLETS-ORDERED :ol
   :BULLETS :ul
   :BULLET :li})

