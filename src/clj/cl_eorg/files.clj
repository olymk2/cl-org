(ns cl-eorg.files
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [cl-eorg.parser :as parser]
            [cl-eorg.core :refer [parse]]
            [cl-eorg.html :refer [org->replacements org->to-hiccup org->split]]))

(defn absolute-path [path]
  (.getCanonicalPath (clojure.java.io/file path)))

(defn normalize-paths [path file]
  {:relative-path (subs file (count path))
   :absolute-path (str file)})

(defn check-files-exist [path]
  (if (.exists path)
    (prn "exists")
    (prn (str "File missing " path))))

(defn modify-map [hm]
  (if (:filetags hm)
    (assoc hm :filetags (-> hm :filetags (clojure.string/split #":")))
    hm))

(defn build-map [path file]
  (->> file
       slurp
       parser/parse
       (parser/filter-root-nodes ["THUMBNAIL" "FILETAGS"])
       modify-map
       ;(map #(assoc % :filetags (-> % :filetags (clojure.string/split #":"))))
       ;(into {})
       (merge (normalize-paths (absolute-path path) file))))

(defn fetch-org-files
  "Will return a list of matching org files under directory, clj only"
  [path]
  (->> (clojure.java.io/file path)
       file-seq
       (filter #(.isFile %))
       (filter #(clojure.string/ends-with? % ".org"))
       (mapv #(.getCanonicalPath %))))

(defn fetch-org-files->map [path]
  (->> (fetch-org-files path)
       (mapv (fn [org-file]
               (try
                 (build-map path org-file)
                 (catch Exception e
                   (tap> (str "Failed to open " org-file  (.getMessage e)))))))))

(defn move-org-files
  "Copy all files "
  [to-path org-map]
  (->> org-map
       (mapv (fn [{:keys [relative-path] :as org-file}]
               (io/make-parents (str to-path relative-path))
               org-file))
       (mapv (fn [{:keys [relative-path absolute-path] :as org-file}]
               (io/copy (io/file absolute-path)
                        (io/file (str to-path relative-path)))
               org-file))))

(defn save-org-meta-index
  ([to-path org-map] (save-org-meta-index "org-meta-index.edn" to-path org-map))
  ([filename to-path org-map]
   (io/make-parents (str to-path filename))
   (clojure.pprint/pprint
    (map #(dissoc % :absolute-path) org-map)
    (io/writer (str to-path filename)))
   org-map))

(comment
  (add-tap prn)
  (normalize-paths "/var/www/docs/resources/public/"
                   "/var/www/docs/resources/public/documents/services.org")

  (fetch-org-files "/var/www/docs/resources/public/documents/")
  (build-map "/var/www/docs/resources/public/documents/" "/var/www/docs/resources/public/documents/services.org")

  (inline-filelist "./resources/public/")
  (inline-filelist-map "./resources/public/")
  (build-map "/home/oly/repos/do-blog/posts/"
             "/home/oly/repos/do-blog/posts/hardware/gaups-test.org")
  (->> (fetch-org-files->map "/home/oly/repos/do-blog/posts/")
       (save-org-meta-index "/home/oly/repos/do-blog/posts/"))
  (fetch-org-files->map "./resources/public/")
  (->> (fetch-org-files->map "/home/oly/repos/do-blog/posts/")
       (save-org-meta-index "/tmp/"))
  (->> (fetch-org-files->map "./resources/public/")
       (move-org-files "/tmp/")))



