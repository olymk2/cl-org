(ns cl-eorg.macros
  (:require [clojure.java.io :as io]
            [cl-eorg.files :refer [fetch-org-files fetch-org-files->map move-org-files save-org-meta-index]]))

;; Some helper macros to fetch org file content or getting a file tree
;; handy javascript side when you cant open files.

(defmacro inline-resource
  "Load file contents into resulting, src files"
  [resource-path]
  (slurp (io/resource resource-path)))

(defmacro inline-filelist
  "can be used js side to inject a list of relative document paths"
  [path]
  (fetch-org-files path))

(defmacro inline-filelist-map
  "can be used js side to inject a list of relative document paths"
  [path]
  (fetch-org-files->map path))

(defmacro inline-generate-map
  "Given a map will write it out to a file"
  [to-path org-map]
  (save-org-meta-index to-path org-map))

(defmacro org-files->to-index-file
  "Given a path generate a map and write to a file"
    [from-path to-path]
  (->> (fetch-org-files->map from-path)
       (save-org-meta-index to-path)))

(defmacro org-doc->move-process
  "Move files and return a list of document maps"
  [from-path to-path]
  (->> (fetch-org-files->map from-path)
       (save-org-meta-index to-path)
       (move-org-files to-path)))


(comment
  (inline-filelist "./" )
  (inline-filelist-map "./" )
  (fetch-org-files->map "./")
  (org-files->to-index "/home/oly/repos/do-blog/posts/" "/tmp/test.edn")
  (->> (inline-filelist "/home/oly/repos/do-blog/posts/" )
       (inline-generate-map "/tmp/test.edn" ))
  (org-doc->move+process "./" "/tmp/")
)
