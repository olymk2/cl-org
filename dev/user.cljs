(ns dev.user
  (:require [clojure.core.protocols :refer [Datafiable]]
            [cl-eorg.parser :as parser]
            [cl-eorg.emphasis :as emphasis]
            [reagent.core :as r]
            [reagent.dom :as dom]
            [hiccups.runtime :as hiccupsrt]
            [ajax.core :refer [GET POST]]
            [clojure.datafy :refer [datafy]]
            [cl-eorg.html :refer [org->replacements org->to-hiccup org->split]]
            [portal.web :as p])
  (:require-macros
   [cl-eorg.macros :refer [inline-resource inline-filelist]]))

;; https://websemantics.uk/articles/displaying-code-in-web-pages/
