(ns ^:figwheel-hooks site.core
  (:require
   [cl-eorg.parser :as parser]
   [reagent.core :as r]
   [reagent.dom :as dom]
   [ajax.core :refer [GET]]
   [cl-eorg.html :refer [org->to-hiccup org->split]])
  (:require-macros
   [cl-eorg.macros :refer [inline-resource inline-filelist]]))


;; https://websemantics.uk/articles/displaying-code-in-web-pages/

(def files (inline-filelist "./resources/public"))
(def debug-org-document (inline-resource "public/documents/debug.org"))
(def simple-org-document (inline-resource "public/documents/test.org"))
(def simple-org-simple-document (inline-resource "public/documents/org-simple.org"))
(def simple-org-headings-document (inline-resource "public/documents/org-headings.org"))

(defonce state (r/atom {:current-doc "" :current-key :org-01}))
(def docs {:org-01 (inline-resource "public/documents/org-cheatsheet-01.org")
           :org-02 (inline-resource "public/documents/org-cheatsheet-02.org")
           :org-03 debug-org-document
           :org-tables (inline-resource "public/documents/org-tables.org")})

(def files-relative
  (->> files
       (map #(clojure.string/split % "/resources/public/"))
       (map second)))

(defn reagent-render [hiccup]
  (dom/render hiccup (js/document.getElementById "app")))

(defn org-render [doc]
  (->> doc
       parser/parse
       org->to-hiccup))

(defn fetch-document [doc]
  (GET doc
    {:handler
     (fn [document]
       (swap! state assoc :current-doc document))}))

(defn page []
  (let [org-doc (r/cursor state [:current-doc])
        org-key (r/cursor state [:current-key])]
    (fn []
      [:div
       [:link {:rel "stylesheet" :href "https://unpkg.com/tachyons@4.12.0/css/tachyons.min.css"}]
       [:div.flex
        ;; file tree
        [:div.w-50.pa3.mr2
         "Using inline file list get available files"
         [:div [:ul
                (map (fn [r]
                       [:li {:key r}
                        [:a {:on-click (fn [e] (fetch-document r))} r]])
                     files-relative)]]]

        [:div.w-50.pa3.mr2
         [:select
          {:value @org-key
           :on-change (fn [e]
                        (reset! org-key (-> e .-target .-value keyword))
                        (reset! org-doc ((-> e .-target .-value keyword) docs)))}
          (map (fn [k] [:option {:key k :value k} k]) (keys docs))]
         [:div.row
          [:textarea.w-100
           {:on-change (fn [e]
                         (.preventDefault e)
                         (reset! org-doc (-> e .-target .-value)))
            :defaultValue @org-doc
            :rows 8}]]]]
       [:div.flex
        [:div.ma4  @(r/track! org-render @org-doc)]]])))


;; reagent render example


;;https://gitlab.com/olymk2/em/-/raw/master/readme.org

;; reagent render example
(reagent-render [page])
